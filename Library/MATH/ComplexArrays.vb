﻿Imports System.Numerics
Imports System.Runtime.CompilerServices

Public Module ComplexArrays

#Region " MAGNITUDE "

    ''' <summary> Calculates magnitudes of the complex values. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> The magnitudes of the complex values. </returns>
    <Extension()>
    Public Function Magnitudes(ByVal values As Complex()) As Double()

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Dim result(values.Length - 1) As Double
            For i As Integer = 0 To values.Length - 1
                result(i) = Complex.Abs(values(i))
            Next i
            Return result
        Else
            Return New Double() {}
        End If

    End Function

#End Region

#Region " ARRAYS "

    ''' <summary> Adds a value to the complex values. </summary>
    ''' <param name="values"> The values. </param>
    ''' <param name="value">  The value. </param>
    <Extension()>
    Public Sub Add(ByVal values() As Complex, ByVal value As Complex)

        If values IsNot Nothing AndAlso values.Length > 0 Then
            For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                values(i) += value
            Next
        End If

    End Sub

    ''' <summary> Returns the index of the first maximum of the complex array. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> Index of the first maximum of the complex array. </returns>
    <Extension()>
    Public Function IndexFirstMaximum(ByVal values() As Complex) As Integer

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Dim maxIndex As Integer = values.GetLowerBound(0)
            Dim max As Double = values(maxIndex).Abs
            For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                Dim abs As Double = values(i).Abs
                If max < abs Then
                    max = abs
                    maxIndex = i
                End If
            Next
            Return maxIndex
        Else
            Return -1
        End If

    End Function

    ''' <summary> Calculates the complex mean of the complex values. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> The complex mean of the complex values. </returns>
    <Extension()>
    Public Function Mean(ByVal values() As Complex) As Complex

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Return Sum(values) / values.Length
        Else
            Return 0
        End If

    End Function

    ''' <summary> Calculates the mean of the real values of the complex values. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> The mean of the real values of the complex values. </returns>
    <Extension()>
    Public Function RealMean(ByVal values() As Complex) As Double

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Return SumReals(values) / values.Length
        Else
            Return 0
        End If

    End Function

    ''' <summary> Removes the mean from the values array. </summary>
    ''' <param name="values"> The values. </param>
    <Extension()>
    Public Sub RemoveMean(ByVal values() As Complex)

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Add(values, -Mean(values))
        End If

    End Sub

    ''' <summary> Removes the mean from the values array. </summary>
    ''' <param name="values"> The values. </param>
    <Extension()>
    Public Sub RemoveRealMean(ByVal values() As Complex)

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Add(values, New Complex(-RealMean(values), 0))
        End If

    End Sub

    ''' <summary> Multiplies the complex values by a scalar. </summary>
    ''' <param name="values"> The values. </param>
    ''' <param name="value">  The value. </param>
    <Extension()>
    Public Sub Scale(ByVal values() As Complex, ByVal value As Complex)

        If values IsNot Nothing AndAlso values.Length > 0 Then
            For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                values(i) *= value
            Next
        End If

    End Sub

    ''' <summary> Multiplies the complex values by a scalar array. </summary>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="values">  The values. </param>
    ''' <param name="scalars"> The scalars. </param>
    <Extension()>
    Public Sub Scale(ByVal values() As Complex, ByVal scalars() As Complex)

        If values IsNot Nothing AndAlso values.Length > 0 AndAlso
            scalars IsNot Nothing AndAlso scalars.Length > 0 Then

            If values.GetLowerBound(0) <> scalars.GetLowerBound(0) Then
                Throw New ArgumentOutOfRangeException("values", values, "The arrays of values and scalars must have the same lower bound")
            ElseIf values.GetUpperBound(0) <> scalars.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException("values", values, "The arrays of values and scalars must have the same upper bound")
            Else
                For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                    values(i) *= scalars(i)
                Next
            End If
        End If

    End Sub

    ''' <summary> Multiplies the complex values by a scalar array. </summary>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="values">  The values. </param>
    ''' <param name="scalars"> The scalars. </param>
    <Extension()>
    Public Sub Scale(ByVal values() As Complex, ByVal scalars() As Double)

        If values IsNot Nothing AndAlso values.Length > 0 AndAlso
            scalars IsNot Nothing AndAlso scalars.Length > 0 Then

            If values.GetLowerBound(0) <> scalars.GetLowerBound(0) Then
                Throw New ArgumentOutOfRangeException("values", values, "The arrays of values and scalars must have the same lower bound")
            ElseIf values.GetUpperBound(0) <> scalars.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException("values", values, "The arrays of values and scalars must have the same upper bound")
            Else
                For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                    values(i) *= scalars(i)
                Next
            End If
        End If

    End Sub

    ''' <summary> Multiplies the complex array real values by a scalar array and zeros the imaginary
    ''' parts. </summary>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="values">  The values. </param>
    ''' <param name="scalars"> The scalars. </param>
    <Extension()>
    Public Sub ScaleReals(ByVal values() As Complex, ByVal scalars() As Double)

        If values IsNot Nothing AndAlso values.Length > 0 AndAlso
            scalars IsNot Nothing AndAlso scalars.Length > 0 Then

            If values.GetLowerBound(0) <> scalars.GetLowerBound(0) Then
                Throw New ArgumentOutOfRangeException("values", values, "The arrays of values and scalars must have the same lower bound")
            ElseIf values.GetUpperBound(0) <> scalars.GetUpperBound(0) Then
                Throw New ArgumentOutOfRangeException("values", values, "The arrays of values and scalars must have the same upper bound")
            Else
                For i As Integer = values.GetLowerBound(0) To values.GetUpperBound(0)
                    values(i) = New Complex(values(i).Real * scalars(i), 0)
                Next
            End If
        End If

    End Sub

    ''' <summary> Swaps values between two array locations. </summary>
    ''' <param name="data"> The array containing the data. </param>
    ''' <param name="i">    An array index. </param>
    ''' <param name="j">    An array index. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="i")>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="j")>
    <Extension()>
    Public Sub Swap(ByVal data() As Complex, ByVal i As Integer, ByVal j As Integer)

        If Not data Is Nothing Then
            Dim cache As Complex = data(i)
            data(i) = data(j)
            data(j) = cache
        End If

    End Sub

    ''' <summary> Calculates the sum of the complex values. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> The sum of the complex values. </returns>
    <Extension()>
    Public Function Sum(ByVal values() As Complex) As Complex

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Dim cache As Complex = 0
            For Each c As Complex In values
                cache += c
            Next
            Return cache
        Else
            Return 0
        End If

    End Function

    ''' <summary> Calculates the sum of the real value elements. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> The sum of the real value elements. </returns>
    <Extension()>
    Public Function SumReals(ByVal values() As Complex) As Double

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Dim cache As Double = 0
            For Each c As Complex In values
                cache += c.Real
            Next
            Return cache
        Else
            Return 0
        End If

    End Function

    ''' <summary> Calculates the sum of the complex values squared. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> The sum of the complex values squared. </returns>
    <Extension()>
    Public Function SumSquares(ByVal values() As Complex) As Double

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Dim cache As Double = 0
            For Each c As Complex In values
                Dim abs As Double = c.Abs
                cache += abs + abs
            Next
        Else
            Return 0
        End If

    End Function

    ''' <summary> Calculates the root mean square (RMS) value of the complex values. </summary>
    ''' <param name="values"> The values. </param>
    ''' <returns> The root mean square (RMS) value of the complex values. </returns>
    <Extension()>
    Public Function RootMeanSquare(ByVal values() As Complex) As Double

        If values IsNot Nothing AndAlso values.Length > 0 Then
            Return Math.Sqrt(SumSquares(values) / values.Length)
        Else
            Return 0
        End If

    End Function

    ''' <summary> Swaps the imaginary and real values. </summary>
    ''' <param name="values"> The values. </param>
    <Extension()>
    Public Sub Swap(ByVal values() As Complex)
        If values IsNot Nothing Then
            For i As Integer = 0 To values.Length - 1
                values(i) = values(i).Swap
            Next
        End If
    End Sub

#End Region

#Region " COPY "

    ''' <summary> Copy values between complex arrays. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="sourceArray">      The source array. </param>
    ''' <param name="destinationArray"> The destination array. </param>
    <Extension()>
    Public Sub CopyTo(ByVal sourceArray() As Complex, ByVal destinationArray() As Complex)

        If sourceArray Is Nothing Then
            Throw New ArgumentNullException("sourceArray")
        End If
        If destinationArray Is Nothing Then
            Throw New ArgumentNullException("destinationArray")
        End If

        If sourceArray.Length > 0 AndAlso destinationArray.Length > 0 Then
            For i As Integer = Math.Max(sourceArray.GetLowerBound(0),
                                        destinationArray.GetLowerBound(0)) To Math.Min(sourceArray.GetUpperBound(0),
                                                                                       destinationArray.GetUpperBound(0))
                destinationArray(i) = sourceArray(i)
            Next
        End If

    End Sub

#End Region

#Region " COPY DOUBLE "

    ''' <summary> Copies the real values to the complex array. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="sourceArray">      The source array. </param>
    ''' <param name="destinationArray"> The destination array. </param>
    <Extension()>
    Public Sub CopyTo(ByVal sourceArray() As Double, ByVal destinationArray() As Complex)

        If sourceArray Is Nothing Then
            Throw New ArgumentNullException("sourceArray")
        End If
        If destinationArray Is Nothing Then
            Throw New ArgumentNullException("destinationArray")
        End If

        If sourceArray.Length > 0 AndAlso destinationArray.Length > 0 Then
            For i As Integer = Math.Max(sourceArray.GetLowerBound(0),
                                        destinationArray.GetLowerBound(0)) To Math.Min(sourceArray.GetUpperBound(0),
                                                                                       destinationArray.GetUpperBound(0))
                destinationArray(i) = New Complex(sourceArray(i), 0)
            Next
        End If
    End Sub

    ''' <summary> Copies the real and imaginary values to the complex array. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="reals">            The real values. </param>
    ''' <param name="imaginaries">      The imaginaries. </param>
    ''' <param name="destinationArray"> The destination array. </param>
    Public Sub Copy(ByVal reals() As Double, ByVal imaginaries() As Double, ByVal destinationArray() As Complex)
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If destinationArray Is Nothing Then
            Throw New System.ArgumentNullException("destinationArray")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length > 0 AndAlso destinationArray.Length > 0 Then
            For i As Integer = Math.Max(reals.GetLowerBound(0),
                                        destinationArray.GetLowerBound(0)) To Math.Min(reals.GetUpperBound(0),
                                                                                       destinationArray.GetUpperBound(0))
                destinationArray(i) = New Complex(reals(i), imaginaries(i))
            Next
        End If
    End Sub

    ''' <summary> Copies the complex values to a real and imaginary arrays. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="sourceArray"> The source array. </param>
    ''' <param name="reals">       The reals. </param>
    ''' <param name="imaginaries"> The imaginaries. </param>
    <Extension()>
    Public Sub CopyTo(ByVal sourceArray() As Complex, ByVal reals() As Double, ByVal imaginaries() As Double)
        If sourceArray Is Nothing Then
            Throw New System.ArgumentNullException("sourceArray")
        End If
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length > 0 AndAlso sourceArray.Length > 0 Then
            For i As Integer = Math.Max(reals.GetLowerBound(0),
                                        sourceArray.GetLowerBound(0)) To Math.Min(reals.GetUpperBound(0),
                                                                                  sourceArray.GetUpperBound(0))
                reals(i) = sourceArray(i).Real
                imaginaries(i) = sourceArray(i).Imaginary
            Next
        End If
    End Sub

    ''' <summary> Copies the complex real values to a real array. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="sourceArray"> The source array. </param>
    ''' <param name="reals">       The reals. </param>
    <Extension()>
    Public Sub CopyTo(ByVal sourceArray() As Complex, ByVal reals() As Double)
        If sourceArray Is Nothing Then
            Throw New System.ArgumentNullException("sourceArray")
        End If
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If reals.Length > 0 AndAlso sourceArray.Length > 0 Then
            For i As Integer = Math.Max(reals.GetLowerBound(0),
                                        sourceArray.GetLowerBound(0)) To Math.Min(reals.GetUpperBound(0),
                                                                                  sourceArray.GetUpperBound(0))
                reals(i) = sourceArray(i).Real
            Next
        End If
    End Sub

#End Region

#Region " COPY SINGLE "

    ''' <summary> Copies the real values to the complex array. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="sourceArray">      The source array. </param>
    ''' <param name="destinationArray"> The destination array. </param>
    <Extension()>
    Public Sub CopyTo(ByVal sourceArray() As Single, ByVal destinationArray() As Complex)

        If sourceArray Is Nothing Then
            Throw New ArgumentNullException("sourceArray")
        End If
        If destinationArray Is Nothing Then
            Throw New ArgumentNullException("destinationArray")
        End If

        If sourceArray.Length > 0 AndAlso destinationArray.Length > 0 Then
            For i As Integer = Math.Max(sourceArray.GetLowerBound(0),
                                        destinationArray.GetLowerBound(0)) To Math.Min(sourceArray.GetUpperBound(0),
                                                                                       destinationArray.GetUpperBound(0))
                destinationArray(i) = New Complex(sourceArray(i), 0)
            Next
        End If
    End Sub

    ''' <summary> Copies the real and imaginary values to the complex array. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="reals">            The reals. </param>
    ''' <param name="imaginaries">      The imaginaries. </param>
    ''' <param name="destinationArray"> The destination array. </param>
    Public Sub Copy(ByVal reals() As Single, ByVal imaginaries() As Single, ByVal destinationArray() As Complex)
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If destinationArray Is Nothing Then
            Throw New System.ArgumentNullException("destinationArray")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length > 0 AndAlso destinationArray.Length > 0 Then
            For i As Integer = Math.Max(reals.GetLowerBound(0),
                                        destinationArray.GetLowerBound(0)) To Math.Min(reals.GetUpperBound(0),
                                                                                       destinationArray.GetUpperBound(0))
                destinationArray(i) = New Complex(reals(i), imaginaries(i))
            Next
        End If
    End Sub

    ''' <summary> Copies the complex values to a real and imaginary arrays. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="sourceArray"> The source array. </param>
    ''' <param name="reals">       The reals. </param>
    ''' <param name="imaginaries"> The imaginaries. </param>
    <Extension()>
    Public Sub CopyTo(ByVal sourceArray() As Complex, ByVal reals() As Single, ByVal imaginaries() As Single)
        If sourceArray Is Nothing Then
            Throw New System.ArgumentNullException("sourceArray")
        End If
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        If reals.Length > 0 AndAlso sourceArray.Length > 0 Then
            For i As Integer = Math.Max(reals.GetLowerBound(0),
                                        sourceArray.GetLowerBound(0)) To Math.Min(reals.GetUpperBound(0),
                                                                                  sourceArray.GetUpperBound(0))
                reals(i) = CSng(sourceArray(i).Real)
                imaginaries(i) = CSng(sourceArray(i).Imaginary)
            Next
        End If
    End Sub

    ''' <summary> Copies the complex real values to a real array. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="sourceArray"> The source array. </param>
    ''' <param name="reals">       The reals. </param>
    <Extension()>
    Public Sub CopyTo(ByVal sourceArray() As Complex, ByVal reals() As Single)
        If sourceArray Is Nothing Then
            Throw New System.ArgumentNullException("sourceArray")
        End If
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If reals.Length > 0 AndAlso sourceArray.Length > 0 Then
            For i As Integer = Math.Max(reals.GetLowerBound(0),
                                        sourceArray.GetLowerBound(0)) To Math.Min(reals.GetUpperBound(0),
                                                                                  sourceArray.GetUpperBound(0))
                reals(i) = CSng(sourceArray(i).Real)
            Next
        End If
    End Sub

#End Region

#Region " CREATE DOUBLE "

    ''' <summary> Creates a complex array from the array of real values. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="reals"> The reals. </param>
    ''' <returns> A complex array from the array of real values. </returns>
    <Extension()>
    Public Function ToComplex(ByVal reals() As Double) As Complex()
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        Dim x(reals.Length - 1) As Complex
        CopyTo(reals, x)
        Return x
    End Function

    ''' <summary> Converts the real values element to a complex array of the specified length. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="reals">        The reals. </param>
    ''' <param name="elementCount"> The element count. </param>
    ''' <returns> A complex array of the specified length. </returns>
    <Extension()>
    Public Function ToComplex(ByVal reals() As Double, ByVal elementCount As Integer) As Complex()
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If elementCount = 0 Then
            Return New Complex() {}
        Else
            Dim x(elementCount - 1) As Complex
            CopyTo(reals, x)
            Return x
        End If
    End Function

    ''' <summary> Converts the real and imaginary values to complex. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="reals">       The reals. </param>
    ''' <param name="imaginaries"> The imaginaries. </param>
    ''' <returns> The complex array of the real and imaginary values. </returns>
    Public Function ToComplex(ByVal reals() As Double, ByVal imaginaries() As Double) As Complex()
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        Dim x(reals.Length - 1) As Complex
        Copy(reals, imaginaries, x)
        Return x
    End Function

    ''' <summary> Converts the real and imaginary values to complex. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. Real and imaginary array size mismatch. </exception>
    ''' <param name="reals">        The reals. </param>
    ''' <param name="imaginaries">  The imaginaries. </param>
    ''' <param name="elementCount"> The element count. </param>
    ''' <returns> The complex array of the real and imaginary values. </returns>
    Public Function ToComplex(ByVal reals() As Double, ByVal imaginaries() As Double, ByVal elementCount As Integer) As Complex()
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        Dim x(elementCount - 1) As Complex
        Copy(reals, imaginaries, x)
        Return x
    End Function

#End Region

#Region " CREATE SINGLE "

    ''' <summary> Creates a complex array from the array of real values. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="reals"> The reals. </param>
    ''' <returns> A complex array from the array of real values. </returns>
    <Extension()>
    Public Function ToComplex(ByVal reals() As Single) As Complex()
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        Dim x(reals.Length - 1) As Complex
        CopyTo(reals, x)
        Return x
    End Function

    ''' <summary> Converts the real values element to a complex array of the specified length. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="reals">        The reals. </param>
    ''' <param name="elementCount"> The element count. </param>
    ''' <returns> The real values element converted to a complex array of the specified length. </returns>
    <Extension()>
    Public Function ToComplex(ByVal reals() As Single, ByVal elementCount As Integer) As Complex()
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If elementCount = 0 Then
            Return New Complex() {}
        Else
            Dim x(elementCount - 1) As Complex
            CopyTo(reals, x)
            Return x
        End If
    End Function

    ''' <summary> Converts the real and imaginary values to complex. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="reals">       The reals. </param>
    ''' <param name="imaginaries"> The imaginaries. </param>
    ''' <returns> The real and imaginary values to complex. </returns>
    Public Function ToComplex(ByVal reals() As Single, ByVal imaginaries() As Single) As Complex()
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        Dim x(reals.Length - 1) As Complex
        Copy(reals, imaginaries, x)
        Return x
    End Function

    ''' <summary> Converts the real and imaginary values to complex. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="reals">        The reals. </param>
    ''' <param name="imaginaries">  The imaginaries. </param>
    ''' <param name="elementCount"> The element count. </param>
    ''' <returns> The real and imaginary values to complex. </returns>
    Public Function ToComplex(ByVal reals() As Single, ByVal imaginaries() As Single, ByVal elementCount As Integer) As Complex()
        If reals Is Nothing Then
            Throw New System.ArgumentNullException("reals")
        End If
        If imaginaries Is Nothing Then
            Throw New System.ArgumentNullException("imaginaries")
        End If
        If reals.Length <> imaginaries.Length Then
            Throw New ArgumentOutOfRangeException("reals", reals, "The arrays of real- and imaginary-parts must have the same size")
        End If
        Dim x(elementCount - 1) As Complex
        Copy(reals, imaginaries, x)
        Return x
    End Function

#End Region

End Module
