﻿Imports System.Numerics
Imports System.Runtime.CompilerServices

''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <summary> Provides simple functions of complex arguments. </summary>
Public Module ComplexExtensions

    ''' <summary> Computes the phase of a complex number. </summary>
    ''' <remarks> <para>The phase of a complex number is the angle between the line joining it to the
    ''' origin and the real axis of the complex plane.</para>
    ''' <para>The phase of complex numbers in the upper complex plane lies between 0 and &#x3C0;. The
    ''' phase of complex numbers in the lower complex plane lies between 0 and -&#x3C0;. The phase of
    ''' a real number is zero.</para> </remarks>
    ''' <param name="z"> The argument. </param>
    ''' <returns> The value of Arg(z). </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    <Extension()>
    Public Function Arg(ByVal z As Complex) As Double
        ' returns 0 to PI in the upper complex plane (Imaginary part>=0),
        ' 0 to -PI in the lower complex plane (Imaginary part<0)
        Return Math.Atan2(z.Imaginary, z.Real)
    End Function

    ''' <summary> Computes the absolute value of a complex number. </summary>
    ''' <remarks> <para>The absolute value of a complex number is the distance of the number from the
    ''' origin in the complex plane. This is a compatible generalization of the definition of the
    ''' absolute value of a real number.</para> </remarks>
    ''' <param name="value"> The argument. </param>
    ''' <returns> The value of |z|. </returns>
    ''' <seealso cref="System.Math.Abs"/>
    <Extension()>
    Public Function Abs(ByVal value As Complex) As Double
        Return ComplexExtensions.Hypotenuse(value.Real, value.Imaginary)
    End Function

    ''' <summary> Gets the complex conjugate of the complex number. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> The complex conjugate of the complex number. </returns>
    <Extension()>
    Public Function Conjugate(ByVal value As Complex) As Complex
        Return (New Complex(value.Real, -value.Imaginary))
    End Function

    ''' <summary> Computes the cosine of a complex number. </summary>
    ''' <param name="z"> The argument. </param>
    ''' <returns> The value of Cos(z). </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    <Extension()>
    Public Function Cos(ByVal z As Complex) As Complex
        Dim p As Double = Math.Exp(z.Imaginary)
        Dim q As Double = 1 / p
        Dim sinwH As Double = (p - q) / 2.0
        Dim cosineH As Double = (p + q) / 2.0
        Return New Complex(Math.Cos(z.Real) * cosineH, -Math.Sin(z.Real) * sinwH)
    End Function

    ''' <summary> Computes the hyperbolic cosine of a complex number. </summary>
    ''' <param name="z"> The argument. </param>
    ''' <returns> The value of CosH(z). </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    <Extension()>
    Public Function Cosh(ByVal z As Complex) As Complex
        Return Cos(New Complex(-z.Imaginary, z.Real))
    End Function

    ''' <summary> Computes e raised to the power of a complex number. </summary>
    ''' <param name="z"> The argument. </param>
    ''' <returns> The value of e<sup>z</sup>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    <Extension()>
    Public Function Exp(ByVal z As Complex) As Complex
        Dim m As Double = Math.Exp(z.Real)
        Return New Complex(m * AdvancedMath.Cos(z.Imaginary, 0.0), m * AdvancedMath.Sin(z.Imaginary, 0.0))
    End Function

    ''' <summary> Computes the length of a right triangle's hypotenuse. </summary>
    ''' <remarks> <para>The length is computed accurately, even in cases where x<sup>2</sup> or
    ''' y<sup>2</sup> would overflow.</para> </remarks>
    ''' <param name="value"> The argument. </param>
    ''' <returns> The length of the hypotenuse, SQRT(real<sup>2</sup> + imaginary<sup>2</sup>) =
    ''' |value|. </returns>
    <Extension()>
    Public Function Hypotenuse(ByVal value As Complex) As Double
        Return ComplexExtensions.Hypotenuse(value.Real, value.Imaginary)
    End Function

    ''' <summary> Computes the length of a right triangle's hypotenuse. </summary>
    ''' <remarks> <para>The length is computed accurately, even in cases where x<sup>2</sup> or
    ''' y<sup>2</sup> would overflow.</para> </remarks>
    ''' <param name="value">      The length of one side. </param>
    ''' <param name="orthogonal"> The length of orthogonal side. </param>
    ''' <returns> The length of the hypotenuse, sqrt(x<sup>2</sup> + y<sup>2</sup>). </returns>
    Public Function Hypotenuse(ByVal value As Double, ByVal orthogonal As Double) As Double
        If (value = 0.0) AndAlso (orthogonal = 0.0) Then
            Return 0.0
        Else
            Dim ax As Double = Math.Abs(value)
            Dim ay As Double = Math.Abs(orthogonal)
            If ax > ay Then
                Dim r As Double = orthogonal / value
                Return ax * Math.Sqrt(1.0 + r * r)
            Else
                Dim r As Double = value / orthogonal
                Return ay * Math.Sqrt(1.0 + r * r)
            End If
        End If
    End Function

    ''' <summary> Gets or sets the unit imaginary number I. </summary>
    ''' <value> The i. </value>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="I")>
    Public ReadOnly Property I() As Complex
        Get
            Return New Complex(0.0, 1.0)
        End Get
    End Property

    ''' <summary> Computes the natural logarithm of a complex number. </summary>
    ''' <param name="z"> The argument. </param>
    ''' <returns> The value of natural log of z. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    <Extension()>
    Public Function Log(ByVal z As Complex) As Complex
        Return New Complex(Math.Log(Abs(z)), Arg(z))
    End Function

    ''' <summary> Raises a complex number to an arbitrary real power. </summary>
    ''' <param name="z"> The argument. </param>
    ''' <param name="p"> The power. </param>
    ''' <returns> The value of z<sup>p</sup>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="p")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    <Extension()>
    Public Function Pow(ByVal z As Complex, ByVal p As Double) As Complex
        Dim m As Double = Math.Pow(Abs(z), p)
        Dim t As Double = Arg(z) * p
        Return New Complex(m * Math.Cos(t), m * Math.Sin(t))
    End Function

    ''' <summary> Raises a real number to an arbitrary complex power. </summary>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="x"> The real base, which must be non-negative. </param>
    ''' <param name="z"> The complex exponent. </param>
    ''' <returns> The value of x<sup>z</sup>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="x")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    <Extension()>
    Public Function Pow(ByVal x As Double, ByVal z As Complex) As Complex
        If x < 0.0 Then
            Throw New ArgumentOutOfRangeException("x")
        End If
        If z = 0.0 Then
            Return 1.0
        End If
        If x = 0.0 Then
            Return 0.0
        End If
        Dim m As Double = Math.Pow(x, z.Real)
        Dim t As Double = Math.Log(x) * z.Imaginary
        Return New Complex(m * Math.Cos(t), m * Math.Sin(t))
    End Function

    ''' <summary> Raises a complex number to an integer power. </summary>
    ''' <param name="z"> The argument. </param>
    ''' <param name="n"> The power. </param>
    ''' <returns> The value of z<sup>n</sup>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="n")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    <Extension()>
    Public Function Pow(ByVal z As Complex, ByVal n As Integer) As Complex

        ' this is a straight-up copy of MoreMath.Pow with x -> z, double -> Complex

        If n < 0 Then
            Return 1.0 / Pow(z, -n)
        End If

        Select Case n
            Case 0
                ' we follow convention that 0^0 = 1
                Return 1.0
            Case 1
                Return z
            Case 2
                ' 1 multiply
                Return z * z
            Case 3
                ' 2 multiplies
                Return z * z * z
            Case 4
                ' 2 multiplies
                Dim z2 As Complex = z * z
                Return z2 * z2
            Case 5
                ' 3 multiplies
                Dim z2 As Complex = z * z
                Return z2 * z2 * z
            Case 6
                ' 3 multiplies
                Dim z2 As Complex = z * z
                Return z2 * z2 * z2
            Case 7
                ' 4 multiplies
                Dim z3 As Complex = z * z * z
                Return z3 * z3 * z
            Case 8
                ' 3 multiplies
                Dim z2 As Complex = z * z
                Dim z4 As Complex = z2 * z2
                Return z4 * z4
            Case 9
                ' 4 multiplies
                Dim z3 As Complex = z * z * z
                Return z3 * z3 * z3
            Case 10
                ' 4 multiplies
                Dim z2 As Complex = z * z
                Dim z4 As Complex = z2 * z2
                Return z4 * z4 * z2
            Case Else
                Return ComplexExtensions.Pow(z, CDbl(n))
        End Select

    End Function

    ''' <summary> Computes the sine of a complex number. </summary>
    ''' <param name="z"> The argument. </param>
    ''' <returns> The value of sin(z). </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    <Extension()>
    Public Function Sin(ByVal z As Complex) As Complex
        Dim p As Double = Math.Exp(z.Imaginary)
        Dim q As Double = 1 / p
        Dim sineH As Double = (p - q) / 2.0
        Dim cosineH As Double = (p + q) / 2.0
        Return New Complex(Math.Sin(z.Real) * cosineH, Math.Cos(z.Real) * sineH)
    End Function

    ''' <summary> Computes the hyperbolic sine of a complex number. </summary>
    ''' <param name="z"> The argument. </param>
    ''' <returns> The value of SinH(z). </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    <Extension()>
    Public Function Sinh(ByVal z As Complex) As Complex
        ' SinH(z) = -i sin(i z)
        Dim sineH As Complex = Sin(New Complex(-z.Imaginary, z.Real))
        Return New Complex(sineH.Imaginary, -sineH.Real)
    End Function

    ''' <summary> Computes the square root of a complex number. </summary>
    ''' <param name="z"> The argument. </param>
    ''' <returns> The square root of the argument. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    <Extension()>
    Public Function Sqrt(ByVal z As Complex) As Complex
        If z.Imaginary = 0 Then
            Return Math.Sqrt(z.Real)
        Else
            Return Pow(z, 0.5)
        End If
    End Function

    ''' <summary> Gets the complex value swapping imaginary and real. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> The complex value swapping imaginary and real. </returns>
    <Extension()>
    Public Function Swap(ByVal value As Complex) As Complex
        Return New Complex(value.Imaginary, value.Real)
    End Function

    ''' <summary> Computes the tangent of a complex number. </summary>
    ''' <param name="z"> The argument. </param>
    ''' <returns> The value of tan(z). </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    <Extension()>
    Public Function Tan(ByVal z As Complex) As Complex
        ' tan z = [sin(2x) + I SinH(2y)]/[Cos(2x) + I CosH(2y)]
        Dim x2 As Double = 2.0 * z.Real
        Dim y2 As Double = 2.0 * z.Imaginary
        Dim p As Double = Math.Exp(y2)
        Dim q As Double = 1 / p
        Dim cosineH As Double = (p + q) / 2.0
        If Math.Abs(z.Imaginary) < 4.0 Then
            Dim sineH As Double = (p - q) / 2.0
            Dim D As Double = Math.Cos(x2) + cosineH
            Return New Complex(Math.Sin(x2) / D, sineH / D)
        Else
            ' when Imaginary(z) gets too large, SinH and CosH individually blow up
            ' but ratio is still ~1, so rearrange to use TanH instead
            Dim F As Double = (1.0 + Math.Cos(x2) / cosineH)
            Return New Complex(Math.Sin(x2) / cosineH / F, Math.Tanh(y2) / F)
        End If
    End Function

    ''' <summary> Computes the hyperbolic tangent of a complex number. </summary>
    ''' <param name="z"> The argument. </param>
    ''' <returns> The value of TanH(z). </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="z")>
    <Extension()>
    Public Function Tanh(ByVal z As Complex) As Complex
        Return Sinh(z) / Cosh(z)
    End Function

End Module

