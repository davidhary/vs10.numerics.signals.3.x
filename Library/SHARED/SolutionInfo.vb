Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices
Imports System.Security.Permissions

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly
<Assembly: AssemblyCompany("Integrated Scientific Resources")> 
<Assembly: AssemblyCopyright("(c) 2005 Integrated Scientific Resources, Inc. All rights reserved.")> 
<Assembly: AssemblyTrademark("Licensed under The MIT License.")> 
<Assembly: Resources.NeutralResourcesLanguage("en-US", Resources.UltimateResourceFallbackLocation.MainAssembly)> 
<Assembly: AssemblyVersion("3.0.*")> 
